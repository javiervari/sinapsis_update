#!/usr/bin/env python3

import os, time, subprocess
from datetime import datetime
import git
import paho.mqtt.client as mqtt

from libraries.db import DataBase
from libraries import configuration

client = mqtt.Client()
client.connect('sinapsis.vikua.com')
configuration.configuration(conf_route="/home/pi/sinapsis/conf.cfg")
ID = configuration.sinapsis['id']
topic = 'vikua/sinapsis/update'


def repository():
	"""
	Executes update in The Sinapsis's Code in host if the repo contains new tags
	"""
	sinapsis_project = '/home/pi/sinapsis'
	update_project = '/home/pi/sinapsis_update'

	repo_sinapsis = git.Repo(sinapsis_project)
	repo_update = git.Repo(update_project)

	tags_sinapsis = sorted(repo_sinapsis.tags, key=lambda t: t.commit.committed_datetime)[-1].name
	tags_update = sorted(repo_update.tags, key=lambda t: t.commit.committed_datetime)[-1].name

	repo_sinapsis.remote().fetch()
	repo_update.remote().fetch()
	
	tags_sinapsis_fetched = sorted(repo_sinapsis.tags, key=lambda t: t.commit.committed_datetime)[-1].name
	tags_update_fetched = sorted(repo_update.tags, key=lambda t: t.commit.committed_datetime)[-1].name

	repo_sinapsis = git.Repo(sinapsis_project)
	repo_update = git.Repo(update_project)

	if tags_sinapsis != tags_sinapsis_fetched:
		try:
			repo_sinapsis.git.merge(tags_sinapsis_fetched)
			message = """Sinapsis repository has been updated to last version.\n
			DETAILS:
			Date: {0}
			Sinapsis ID: {1},
			Version: {2}""".format(
				datetime.now().strftime('%Y-%m-%d %H:%M %p'),
				ID,
				tags_sinapsis_fetched
			)
			print(message)
			client.publish(topic, message, qos=1)
		except Exception as e:
			print(e)
	else:
		print('Sinapsis: NO')


	if tags_update != tags_update_fetched:
		try:
			repo_update.git.merge(tags_update_fetched)
			message = """Sinapsis_Update repository has been updated to last version.\n
			DETAILS:
			Date: {0},
			Sinapsis ID: {1},
			Version: {2}		
			""".format(
				datetime.now().strftime('%Y-%m-%d %H:%M %p'),
				ID,
				tags_update_fetched
				)
			print(message)
			client.publish(topic, message, qos=1)
		except Exception as e:
			print(e)
	else:
		print("Sinapsis_Update: NO")


def migrations():
	"""
	Updates the DB if the repo contains new .SQL files.
	"""
	
	c=False #Variable to check if this have updates (SO DUMP !)
	db = DataBase(host='localhost',db='sinapsis',user='pi',password='Password123')

	#Local Migrations
	dirMigrations = list(filter(lambda i: i.endswith('.sql'), os.listdir('/home/pi/sinapsis_update/migrations')))
	dirMigrations.sort()

	#DB Migrations
	dbMigrations = db.migrations()

	if dirMigrations != dbMigrations:
		# Se restan las dos listas y el resultado se pasa al metodo de insert migrations
		for file in dirMigrations:
			if file not in dbMigrations:
				print("Inserting path: {} in Migrations table".format(file))
				db.insert_migration(file)


	#Query pending migrations
	pending_migrations=db.pending_migrations()
	for file in pending_migrations:
		c=True
		
		print ("YES... Executing pending file: {}".format(file))
		sqlfile = open('/home/pi/sinapsis_update/migrations/{}'.format(file), 'r')
		out = db.execute_pending(sql_file=sqlfile)
		if out == True:
			db.migration_successful(version=file)
			message = """Migration executed successfully.\n
			DETAILS:
			Date: {0},
			Sinapsis ID: {1},
			Migration: {2}		
			""".format(
				datetime.now().strftime('%Y-%m-%d %H:%M %p'),
				ID,
				file
				)
			print(message)
			client.publish(topic, message, qos=1)
		else:
			print("Something went wrong when executing the {} file.\nDetails: {}".format(file, out))


	if c==False: print("NO")


def requirements():
	with open('/home/pi/sinapsis/requirements.txt') as f:
		requirements = f.readlines()

	requirements_pulled = [x.strip() for x in requirements]
	installed = subprocess.run(['pip3', 'freeze'], stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')

	newPackages = [item for item in requirements_pulled if item not in installed]

	
	if newPackages:
		try:
			print("YES")
			print("New packages to install: {}".format(newPackages))
			for p in newPackages:
				os.system('sudo pip3 install {}'.format(p))

			message = """Packages installed successfully.\n
				DETAILS:
				Date: {0},
				Sinapsis ID: {1},
				Packages: {2}		
				""".format(
					datetime.now().strftime('%Y-%m-%d %H:%M %p'),
					ID,
					newPackages
					)
			print(message)
			client.publish(topic, message, qos=1)
		except Exception as e:
			print(e)
	
	else:
		print("NO")





def main():
	while True:
		time.sleep(5)
		print('REPOSITORY UPDATES ?')
		repository()
		print('\n')
		print('MIGRATION UPDATES ?')
		migrations()
		print('\n')
		print('REQUIREMENT UPDATES ?')
		requirements()
		print('-------------------------------------------\n\n')




if __name__ == '__main__':
	main()
