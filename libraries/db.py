import psycopg2


class DataBase():
    def __init__(self, db, user, password, host):
        self.conn = psycopg2.connect(
                                    host=host,
                                    dbname=db,
                                    user=user,
                                    password=password,
                                        )
        self.conn.autocommit = True
        self.cur = self.conn.cursor()

    def query(self, query):
        try:
            self.cur.execute(query)
            result = self.cur.fetchall()
            return result
        except Exception as e:
            print(e)

    def insert_migration(self, version):
        query = "INSERT INTO SINAPSIS.MIGRATIONS (ID, VERSION) VALUES (NEXTVAL('SINAPSIS.MIGRATIONS_ID_SEQ'), '{}')".format(version)
        try:
            self.cur.execute(query)
        except Exception as e:
            print(e)

    def migrations(self):
        query = "SELECT VERSION FROM SINAPSIS.MIGRATIONS"
        try:
            self.cur.execute(query)
            result = [r[0] for r in self.cur.fetchall()]
            result.sort()
            return result
        except Exception as e:
            print(e)

    def pending_migrations(self):
        """Returns migrations"""
        query = "SELECT VERSION FROM sinapsis.MIGRATIONS WHERE MIGRATED = FALSE"
        self.cur.execute(query)
        result = [r[0] for r in self.cur.fetchall()]
        result.sort()
        return result


    def migration_successful(self, version):
        query = "UPDATE SINAPSIS.MIGRATIONS SET MIGRATED = TRUE WHERE VERSION = '{}'".format(version)
        try:
            self.cur.execute(query)
            return True
        except Exception as e:
            print(e)

    def execute_pending(self, sql_file):
        try:
            self.cur.execute(sql_file.read())
            return True
        except Exception as e:
            return e

    def close(self):
        self.cur.close()
        self.conn.close()
