import os
import ast
from configparser import ConfigParser
import json

sinapsis = {}


def configuration(conf_route):
    global sinapsis
    config = ConfigParser()
    config.read(conf_route)
    for section in config.sections():
        for item in config.items(section):
            if section == "sinapsis":
                sinapsis[item[0]] = item[1]
                    
    return {
        "sinapsis": sinapsis
    }


if __name__ == "__main__":
    configuration()
